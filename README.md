# What's This

Art Collection Theme is a art/photo gallery theme for Hugo. It's not for It's not for publishing blog-posts.

# How To Use

## Download

1. Create a hugo project. 
2. Go to themes folder. 
3. Clone this theme.

```
cd themes
$ git clone https://gitlab.com/mertbakir/art-collection-theme.git
```

## Start

1. Go to ```exampleSite``` and copy ```config.toml``` and ```ng2_settings.json``` to the root directory of your hugo project. 
2. Open ```config.toml``` and add your relevant information.
3. Edit ```ng2_settings.json``` if you want to customize it. See the [docs nanogallery2](https://nanogallery2.nanostudio.org/documentation.html)
4. Place your image gallery in ```/content```.

# Notes

## Details if You Care

You can store photos in the structure below under ```/content```:

- ```/content/birds```
- ```/content/animals/cats```
- ```/content/animals/dogs```

```animals``` is a ```Section``` for Hugo. While ```birds```, ```cats``` and ```dogs``` are treated as ```Page```s. You need to create ```index.md``` under these folders so that Hugo treat them as pages. Images are ```resources``` of these pages.

You don't have to add front-matter in ```index.md``` and empty file will do just fine. Yet, you may want to add information about pages.

An example front-matter:

```yaml
---
title: ""
description: ""
display_summary: false
custom_date: ""
location: ""
artist: ""
copyright: "Public Domain"
relatedlink: ""
linktext: ""
---
```

## More Details


- This theme is using [nanogallery2](https://nanogallery2.nanostudio.org/) which is licensed under GPLv3 for **non-commercial, personal or open source projects**.

---

- You can't use this theme to publish articles, only images.
- Nanogallery2 is only used in ```list_photos.html``` partial which is only used in ```single.html```.
- Section and Home pages are pure hugo.
- Section thumbnails are generated from the first page's first image.
- Page thumbnails are generated from the first image.
- ["taxonomy", "term", "RSS", "sitemap", "robotsTXT", "404"] are disabled.

---

- There should be an ```index.md``` in the image folder. So this makes it a Hugo page, and images are the resources of that page.
- ```index.md``` can be empty or may contain ```front-matter```.
- You can use sections or pages under content. In other words, you can have image folders or group (folder) of image folders in ```content```
- Directory names may contain space.
- If there is no ```title``` set in the front-matter, theme will use ```directory name```.

## What's Missing

I won't work on these, because I don't think they are necessary.

- metadata about images from .yaml front-matter.
- exif info.

## One May Consider Adding These Features

- Utilize nanogallery across the site instead of grouping images by hugo section.
- Utilize hugo tags, and categories ?
- Infinite number of nested folders, instead of 2 level sections.
- Use nanogallery2's breadcrumb?


# License

This project is open-sourced and licensed under the terms of the MIT license. I would be happy though, if you give attribution. <3